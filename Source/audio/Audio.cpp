//
//  Audio.cpp
//  JuceBasicWindow
//
//  Created by Leonard Terry on 16/11/2016.
//
//

#include "Audio.hpp"

Audio::Audio()
{
    //set default oscillatorPtr
    oscillatorPtr = new SinOscillator();

    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    env->setAttackRate(0.1 * getSampleRate()); //0.1 seconds
    env->setDecayRate(0.1 * getSampleRate()); //0.1 seconds
    env->setSustainLevel(0.0);
}




Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
    
    delete oscillatorPtr.get();
}




void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
}




void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                   int numInputChannels,
                                   float** outputChannelData,
                                   int numOutputChannels,
                                   int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    
    while(numSamples--)
    {
        float outputSample = oscillatorPtr.get()->nextSample();
        
        *outL = outputSample * env->process();
        *outR = outputSample * env->process();
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}




void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    oscillatorPtr.get()->setSampleRate(device->getCurrentSampleRate());
    sampleRate = device->getCurrentSampleRate();
}




double Audio::getSampleRate () const
{
    return sampleRate;
}




void Audio::audioDeviceStopped()
{
    
}




void Audio::setOscillatorWaveform(int waveformItemIndex)
{
    switch (waveformItemIndex) {
        case 0:
            oscillatorPtr = new SinOscillator();
            break;
        case 1:
            oscillatorPtr = new SquareOscillator();
            break;
        case 2:
            oscillatorPtr = new SawtoothOscillator();
            break;
        case 3:
            oscillatorPtr = new TriangleOscillator();
            break;
        default:
            break;
    }
}




void Audio::beep(int currentNote)
{
    // set new frequency and amplitude for beep
    oscillatorPtr.get()->setFrequency(juce::MidiMessage::getMidiNoteInHertz(currentNote));
    oscillatorPtr.get()->setAmplitude(amplitude);
    
    env->setAttackRate(attack * getSampleRate());
    env->setDecayRate(decay * getSampleRate());
    env->reset();
    env->gate(true);
}
