//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Leonard Terry on 07/11/2016.
//
//

#include "Counter.hpp"

Counter::Counter() : Thread("CounterThread")
{
    reset();
}




Counter::~Counter()
{
    stopCounterThread();
}




void Counter::reset()
{
    counterValue = 0;
    counterRange = 0;
}




void Counter::startCounterThread()
{
    startThread(); //start run function
}




void Counter::stopCounterThread()
{
    stopThread(1000); //stop run function in 1 second.
}




void Counter::run()
{
    //reset counter value each time thread is started
    counterValue = -1;
    
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
       
        //counter runs whilst there are notes in the array
        if ( counterRange != 0)
        {
            // use a listener list for counter increment
            listenerList.call(&Listener::counterChanged, counterValue++);
            
            //pause loop for time interval length (tempo & rate)
            Time::waitForMillisecondCounter(time + getTrueTimeInterval());
        }
    
        //reset counter if it reaches note array size
        if(counterValue >= counterRange - 1)
            counterValue = -1;
    }
}




void Counter::setTrueTimeInterval()
{
    trueTimeInterval = (60000.0 / tempoTimeInterval) / counterRate;
}
