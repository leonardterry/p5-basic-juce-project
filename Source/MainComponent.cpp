/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent(Audio& a, Sequence& s) :       audio (a),
                                                            transportGUI(s),
                                                            oscillatorGUI(a, s),
                                                            patternGUI(s),
                                                            keyboardGUI(s)
{
    setSize (800, 600);
    
    addAndMakeVisible(transportGUI);
    addAndMakeVisible(oscillatorGUI);
    addAndMakeVisible(patternGUI);
    addAndMakeVisible(keyboardGUI);

    oscillatorGUI.passReference(&keyboardGUI);
    transportGUI.passReference(&oscillatorGUI);
    keyboardGUI.passReference(&patternGUI);
    
    resized();
}




MainComponent::~MainComponent()
{
    
}




void MainComponent::resized()
{
    oscillatorGUI.setBoundsRelative(0.05, 0.2, 0.425, 0.3);
    transportGUI.setBoundsRelative(0.05, 0.05, 0.9, 0.1);
    patternGUI.setBoundsRelative(0.525, 0.2, 0.425, 0.3);
    keyboardGUI.setBoundsRelative(0.05, 0.55, 0.9, 0.4);
    
}




void MainComponent::paint (Graphics& g)
{
    
}




//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}