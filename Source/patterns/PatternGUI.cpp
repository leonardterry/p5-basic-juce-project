//
//  PatternGUI.cpp
//  JuceBasicWindow
//
//  Created by Leonard Terry on 16/11/2016.
//
//

#include "PatternGUI.hpp"

PatternGUI::PatternGUI(Sequence& s) :   sequence(s),
                                        noteArray(s.getNoteArray()),
                                        counter(s.getCounter())
{
    initialise();
    
    octaveButton[2].setToggleState(true, dontSendNotification);
    patternButton[0].setToggleState(true, dontSendNotification);
    
    resized();
}




PatternGUI::~PatternGUI()
{
    
}




void PatternGUI::resized()
{
    octaveButton[0].setBoundsRelative(0.0, 0.0, 0.2, 0.3);
    octaveButton[1].setBoundsRelative(0.2, 0.0, 0.2, 0.3);
    octaveButton[2].setBoundsRelative(0.4, 0.0, 0.2, 0.3);
    octaveButton[3].setBoundsRelative(0.6, 0.0, 0.2, 0.3);
    octaveButton[4].setBoundsRelative(0.8, 0.0, 0.2, 0.3);
    
    patternButton[0].setBoundsRelative(0.0, 0.35, 0.475, 0.3);
    patternButton[1].setBoundsRelative(0.525, 0.35, 0.475, 0.3);
    patternButton[2].setBoundsRelative(0.0, 0.7, 0.475, 0.3);
    patternButton[3].setBoundsRelative(0.525, 0.7, 0.475, 0.3);
}




void PatternGUI::initialise()
{
    StringArray octaveItems = {"-2", "-1", "0", "+1", "+2" };
    for (int c = 0; c < numOctaves; c++) {
        octaveButton[c].setButtonText(octaveItems[c]);
        octaveButton[c].setClickingTogglesState(true);
        octaveButton[c].setColour(TextButton::buttonColourId, Colour(0xFF34495E));
        octaveButton[c].setColour(TextButton::textColourOffId, Colour(0xFFBFBFBF));
        octaveButton[c].setColour(TextButton::textColourOnId, Colour(0xFFECF0F1));
        octaveButton[c].setColour(TextButton::buttonOnColourId, Colour(0xFFF5AB35));
        octaveButton[c].addListener(this);
        addAndMakeVisible(octaveButton[c]);
        
        if (c == 0 )
            octaveButton[c].setConnectedEdges(Button::ConnectedOnRight);
        else if (c == 4)
            octaveButton[c].setConnectedEdges(Button::ConnectedOnLeft);
        else
            octaveButton[c].setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    }
    
    StringArray patternItems = {"Original", "Alt", "Up", "Down" };
    for (int c = 0; c < numPatterns; c++) {
        patternButton[c].setButtonText(patternItems[c]);
        patternButton[c].setClickingTogglesState(true);
        patternButton[c].setColour(TextButton::buttonColourId, Colour(0xFF34495E));
        patternButton[c].setColour(TextButton::textColourOffId, Colour(0xFFBFBFBF));
        patternButton[c].setColour(TextButton::textColourOnId, Colour(0xFFECF0F1));
        patternButton[c].setColour(TextButton::buttonOnColourId, Colour(0xFF19B5FE));
        patternButton[c].addListener(this);
        addAndMakeVisible(patternButton[c]);
    }
}




void PatternGUI::buttonClicked (Button* button)
{
    highlightSelectedButton(button);
    
    setOctave(button);
    
    setNoteArray(button);
    
    setCounterRange(button);
}




void PatternGUI::highlightSelectedButton(Button* button)
{
    if (button == &octaveButton[0] || button == &octaveButton[1] || button == &octaveButton[2] || button == &octaveButton[3] || button == &octaveButton[4])
    {
        for (int c = 0; c < numOctaves; c++){
            
            //set all un-selected states to false and selected button state to true
            if (button != &octaveButton[c])
                octaveButton[c].setToggleState(false, dontSendNotification);
            else
                octaveButton[c].setToggleState(true, dontSendNotification);
        }
    }
    
    else if (button == &patternButton[0] || button == &patternButton[1] || button == &patternButton[2] || button == &patternButton[3])
    {
        for (int c = 0; c < numPatterns; c++) {
            
            //set all un-selected states to false and selected button state to true
            if (button != &patternButton[c])
                patternButton[c].setToggleState(false, dontSendNotification);
            else
                patternButton[c].setToggleState(true, dontSendNotification);
        }
    }
}




void PatternGUI::setOriginalPattern()
{
    for (int c = 0; c < numPatterns; c++)
        patternButton[c].setToggleState(false, dontSendNotification);
    
    patternButton[0].setToggleState(true, dontSendNotification);
}




void PatternGUI::setOctave ( Button* button)
{
    //if the octave toggle state has been set to true, set the current octave in sequence
    if (octaveButton[0].getToggleState() == true)
        sequence.setSequenceOctave(-2*octave);
    else if (octaveButton[1].getToggleState() == true)
        sequence.setSequenceOctave(-octave);
    else if (octaveButton[2].getToggleState() == true)
        sequence.setSequenceOctave(0);
    else if (octaveButton[3].getToggleState() == true)
        sequence.setSequenceOctave(octave);
    else if (octaveButton[4].getToggleState() == true)
        sequence.setSequenceOctave(2*octave);
}




void PatternGUI::setNoteArray(Button* button)
{
    if (button == &patternButton[0])
        noteArray.setOriginalArray();
    
    else if (button == &patternButton[1])
        noteArray.setAlternateArray();
    
    else if (button == &patternButton[2])
        noteArray.setUpArray();
    
    else if (button == &patternButton[3])
        noteArray.setDownArray();
}




void PatternGUI::setCounterRange(Button* button)
{
    if (button != &patternButton[1])
        counter.setCounterRange(noteArray.getOriginalNoteArraySize());
    else
        counter.setCounterRange(noteArray.getAlternateNoteArraySize());
}