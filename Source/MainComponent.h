/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.hpp"
#include "KeyboardGUI.hpp"
#include "TransportGUI.hpp"
#include "PatternGUI.hpp"
#include "OscillatorGUI.hpp"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel

{
public:
    //==============================================================================
    MainComponent(Audio& a, Sequence& s);
    
    ~MainComponent();

    void resized() override;
    
    void paint (Graphics& g) override;
    
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
private:
    Audio& audio;
    
    TransportGUI transportGUI;
    OscillatorGUI oscillatorGUI;
    PatternGUI patternGUI;
    KeyboardGUI keyboardGUI;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
