//
//  TransportGUI.cpp
//  JuceBasicWindow
//
//  Created by Leonard Terry on 16/11/2016.
//
//

#include "TransportGUI.hpp"

TransportGUI::TransportGUI(Sequence& s) :   sequence(s),
                                            counter(s.getCounter())
{
    StringArray rateItems = {"1/4 crotchet", "1/8 quaver", "1/16 semiquaver"};
    
    // Start / Stop Button
    startStopButton.setButtonText("Start/Stop");
    startStopButton.setClickingTogglesState(true);
    startStopButton.setColour(TextButton::buttonColourId, Colour(0xFF2ECC71));
    startStopButton.setColour(TextButton::buttonOnColourId, Colour(0xFFE74C3C));
    addAndMakeVisible(startStopButton);
    startStopButton.addListener(this);
    
    // Tempo Slider
    tempoSlider.setSliderStyle(juce::Slider::LinearHorizontal);
    tempoSlider.setRange(60.0, 200.0, 1.0);
    tempoSlider.setValue(100);
    tempoSlider.setColour(Slider::backgroundColourId, Colour(0xFF34495E));
    tempoSlider.setColour(Slider::trackColourId, Colour(0xFFECF0F1));
    tempoSlider.setColour(Slider::thumbColourId, Colour(0xFFBFBFBF));
    tempoSlider.setColour(Slider::textBoxTextColourId, Colour(0xFFECF0F1));
    tempoSlider.setColour(Slider::textBoxBackgroundColourId, Colour(0xFF34495E));
    tempoSlider.setColour(Slider::textBoxOutlineColourId, Colour(0xFF34495E));
    tempoSlider.setColour(Slider::textBoxHighlightColourId, Colour(0xFFF5D76E));
    
    addAndMakeVisible(tempoSlider);
    tempoSlider.addListener(this);
    
    // rate ComboBox
    rateComboBox.addItemList(rateItems, 1);
    rateComboBox.setSelectedId(1);
    rateComboBox.setColour(ComboBox::backgroundColourId, Colour(0xFF34495E));
    rateComboBox.setColour(ComboBox::outlineColourId, Colour(0xFF34495E));
    rateComboBox.setColour(ComboBox::textColourId, Colour(0xFFECF0F1));
    addAndMakeVisible(rateComboBox);
    rateComboBox.addListener(this);
    
    // set default tempo and rate values for counter, to initialise before OscillatorGUI.
    counter.setTempo(tempoSlider.getValue());
    counter.setRate(1.0);
    counter.setTrueTimeInterval();
    
    resized();
}




TransportGUI::~TransportGUI()
{
    
}




void TransportGUI::resized()
{
    startStopButton.setBoundsRelative(0.0, 0.0, 0.30, 1.0);
    tempoSlider.setBoundsRelative(0.35, 0.0, 0.30, 1.0);
    rateComboBox.setBoundsRelative(0.70, 0.0, 0.30, 1.0);
}




void TransportGUI::buttonClicked (Button* button)
{
    if (button == &startStopButton)
    {
        if (sequence.getSequenceRunningState() == true)
            sequence.stopSequence();
        else
            sequence.startSequence();
    }
}




void TransportGUI::sliderValueChanged (Slider* slider)
{
    if (slider == &tempoSlider)
    {
        sequence.setSequenceTempo(tempoSlider.getValue());
        counter.setTrueTimeInterval();
        oscillatorGUI->setAttDecSliderRange();
    }
}




void TransportGUI::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    if (comboBoxThatHasChanged == &rateComboBox)
    {
        switch (rateComboBox.getSelectedItemIndex()) {
            case 0:
                sequence.setSequenceRate(1.0);
                counter.setTrueTimeInterval();
                oscillatorGUI->setAttDecSliderRange();
                break;
            case 1:
                sequence.setSequenceRate(2.0);
                counter.setTrueTimeInterval();
                oscillatorGUI->setAttDecSliderRange();
                break;
            case 2:
                sequence.setSequenceRate(4.0);
                counter.setTrueTimeInterval();
                oscillatorGUI->setAttDecSliderRange();
                break;
            default:
                break;
        }
    }
}

