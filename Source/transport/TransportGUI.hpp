//
//  TransportGUI.hpp
//  JuceBasicWindow
//
//  Created by Leonard Terry on 16/11/2016.
//
//

#ifndef TransportGUI_hpp
#define TransportGUI_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Sequence.hpp"
#include "OscillatorGUI.hpp"


/**
    This class creates the UI for all transport related settings.
 */

class TransportGUI :    public Component,
                        public Button::Listener,
                        public Slider::Listener,
                        public ComboBox::Listener
{
    
public:
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /** Constructor */
    TransportGUI(Sequence& s);
    
    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /** Destructor */
    ~TransportGUI();
    
    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /**
        This function passes a pointer reference to MainComponent, to be used by OscillatorGUI
        MainComponent calls this function in its constructor to complete the reference.
     */
    void passReference(OscillatorGUI *ref)                            { oscillatorGUI = ref; }
    
    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /**
        Resizes the components within the class.
     */
    void resized() override;
    
    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /**
        When the button is clicked it checks if the sequence is running. If the sequence is already running 
        then it will stop the sequence, If the sequence is not running then it will start the sequence.
        @see    Sequence
     */
    void buttonClicked (Button* button) override;
    
    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /**
        When the slider value is changed it sets the sequence tempo with its own slider value (slider range 
        is specified in constructor), updates Counter's trueTimeInterval and also triggers 
        OscillatorGUI::setAttDecSliderRange() function.
        @see    Sequence, OscillatorGUI, Counter
     */
    void sliderValueChanged (Slider* slider) override;
    
    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /**
        When the combo-box value is changed it sets the sequence rate, updates Counter's trueTimeInterval 
        and also triggers OscillatorGUI::setAttDecSliderRange() function.
        @see    Sequence, OscillatorGUI, Counter
     */
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    
    
private:
    TextButton startStopButton;
    Slider tempoSlider;
    ComboBox rateComboBox;
    
    Sequence& sequence;
    OscillatorGUI* oscillatorGUI;
    Counter& counter;
};

#endif /* TransportGUI_hpp */
