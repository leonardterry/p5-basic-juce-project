//
//  SquareOscillator.h
//  JuceBasicAudio
//
//  Created by Leonard Terry on 10/11/2016.
//
//

#ifndef SquareOscillator_h
#define SquareOscillator_h

#include "Oscillator.hpp"

/**
    Class for a squarewave oscillator
 */

class SquareOscillator : public Oscillator
{
public:
    
    /**
        Function that provides the execution of the waveshape
     */
    float renderWaveShape (const float currentPhase) override;
};

#endif /* SquareOscillator_h */
