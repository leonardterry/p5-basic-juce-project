//
//  SawtoothOscillator.cpp
//  JuceBasicAudio
//
//  Created by Leonard Terry on 10/11/2016.
//
//

#include "SawtoothOscillator.hpp"

float SawtoothOscillator::renderWaveShape (const float currentPhase)
{
    float out;
    
    if (currentPhase == 0.0){
        out = -1.0;
    }
    else if (currentPhase > 0.0 && currentPhase <= 2.0 * M_PI){
        out = (currentPhase / M_PI) - 1.0;
    }
    
    return out * 0.4;
}