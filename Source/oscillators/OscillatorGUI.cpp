//
//  OscillatorGUI.cpp
//  JuceBasicWindow
//
//  Created by Leonard Terry on 16/11/2016.
//
//

#include "OscillatorGUI.hpp"

OscillatorGUI::OscillatorGUI(Audio& a, Sequence& s) :   audio (a),
                                                        sequence (s),
                                                        noteArray(s.getNoteArray()),
                                                        counter(s.getCounter())
{
    initialiseComboBox();
    initialiseSliders();
    initialiseButtons();
    initialiseLabels();
}




OscillatorGUI::~OscillatorGUI()
{
    
}




void OscillatorGUI::resized()
{
    oscillatorComboBox.setBoundsRelative(0.0, 0.0, 1.0, 0.3);
    
    muteButton.setBoundsRelative(0.0, 0.35, 0.35, 0.3);
    resetButton.setBoundsRelative(0.0, 0.7, 0.35, 0.3);
    
    noteAmplitudeLabel.setBoundsRelative(0.375, 0.35, 0.2, 0.3);
    noteAmplitudeSlider.setBoundsRelative(0.525, 0.35, 0.475, 0.3);
    
    attackLabel.setBoundsRelative(0.425, 0.7, 0.1, 0.3);
    attackSlider.setBoundsRelative(0.475, 0.7, 0.225, 0.3);
    
    decayLabel.setBoundsRelative(0.725, 0.7, 0.1, 0.3);
    decaySlider.setBoundsRelative(0.775, 0.7, 0.225, 0.3);
}




void OscillatorGUI::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    if (comboBoxThatHasChanged == &oscillatorComboBox)
    {
        switch (oscillatorComboBox.getSelectedItemIndex()) {
            case 0:
                audio.setOscillatorWaveform(oscillatorComboBox.getSelectedItemIndex());
                break;
            case 1:
                audio.setOscillatorWaveform(oscillatorComboBox.getSelectedItemIndex());
                break;
            case 2:
                audio.setOscillatorWaveform(oscillatorComboBox.getSelectedItemIndex());
                break;
            case 3:
                audio.setOscillatorWaveform(oscillatorComboBox.getSelectedItemIndex());
                break;
            default:
                break;
        }
    }
}




void OscillatorGUI::sliderValueChanged (Slider* slider)
{
    if (slider == &noteAmplitudeSlider)
    {
        muteButton.setToggleState(false, dontSendNotification);
        audio.setAmplitude(noteAmplitudeSlider.getValue());
    }
    
    if (slider == &attackSlider)
    {
        audio.setAttack(attackSlider.getValue());
        if (decaySlider.getValue() > decaySlider.getMaximum() - attackSlider.getValue())
            decaySlider.setValue(decaySlider.getMaximum() - attackSlider.getValue());
    }

    if (slider == &decaySlider)
    {
        audio.setDecay(decaySlider.getValue());
        if (attackSlider.getValue() > attackSlider.getMaximum() - decaySlider.getValue())
            attackSlider.setValue(attackSlider.getMaximum() - decaySlider.getValue());
    }
}




void OscillatorGUI::buttonClicked (Button* button)
{
    if (button == &resetButton)
    {
        keyboardGUI->resetKeyboard();
        counter.reset();
        noteArray.resetAllArrays();
    }
    if (button == &muteButton)
    {
        if(muteButton.getToggleStateValue() == true)
            audio.setAmplitude(0.0);
        else
            audio.setAmplitude(noteAmplitudeSlider.getValue());
    }
}




void OscillatorGUI::setAttDecSliderRange()
{
    float oldMax = attMax;                                          //assign old values
    float oldAttVal = attackSlider.getValue();
    float oldDecVal = decaySlider.getValue();
    
    attMax = (counter.getTrueTimeInterval() / 1000.0) - 0.075;      //assign new values
    float decMax = attMax;
    float minVal = 0.0375;
    
    float multiplier = attMax / oldMax;                             //calculate multiplier -> new / old
    
    float newAttVal = oldAttVal * multiplier;
    float newDecVal = oldDecVal * multiplier;

    attackSlider.setValue(newAttVal);                               //assign new slider values
    decaySlider.setValue(newDecVal);
    attackSlider.setRange(minVal, attMax);                          //assign new ranges
    decaySlider.setRange(minVal, decMax);

}




void OscillatorGUI::initialiseComboBox()
{
    StringArray oscillatorItems = {"Sine", "Square", "Sawtooth", "Triangle"};
    oscillatorComboBox.addItemList(oscillatorItems, 1);
    oscillatorComboBox.setSelectedId(1);
    oscillatorComboBox.setColour(ComboBox::backgroundColourId, Colour(0xFF34495E));
    oscillatorComboBox.setColour(ComboBox::outlineColourId, Colour(0xFF34495E));
    oscillatorComboBox.setColour(ComboBox::textColourId, Colour(0xFFECF0F1));
    oscillatorComboBox.addListener(this);
    addAndMakeVisible(oscillatorComboBox);
}




void OscillatorGUI::initialiseSliders()
{
    noteAmplitudeSlider.setSliderStyle(juce::Slider::LinearHorizontal);
    noteAmplitudeSlider.setTextBoxStyle(Slider::NoTextBox, true, true, true);
    noteAmplitudeSlider.setColour(Slider::trackColourId, Colour(0xFFBF55EC));
    noteAmplitudeSlider.setColour(Slider::thumbColourId, Colour(0xFF674172));
    noteAmplitudeSlider.setRange(0.0, 1.0);
    noteAmplitudeSlider.setValue(0.5);
    noteAmplitudeSlider.addListener(this);

    attackSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
    attackSlider.setTextBoxStyle(Slider::NoTextBox, true, true, true);
    attackSlider.setColour(Slider::rotarySliderFillColourId, Colour(0xFFBF55EC));
    attackSlider.setColour(Slider::rotarySliderOutlineColourId, Colour(0xFF674172));
    attackSlider.addListener(this);
    
    decaySlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
    decaySlider.setTextBoxStyle(Slider::NoTextBox, true, true, true);
    decaySlider.setColour(Slider::rotarySliderFillColourId, Colour(0xFFBF55EC));
    decaySlider.setColour(Slider::rotarySliderOutlineColourId, Colour(0xFF674172));
    decaySlider.addListener(this);
    
    attackSlider.setValue(0.3);
    decaySlider.setValue(0.3);
    setAttDecSliderRange();

    addAndMakeVisible(noteAmplitudeSlider);
    addAndMakeVisible(attackSlider);
    addAndMakeVisible(decaySlider);
}




void OscillatorGUI::initialiseButtons()
{
    muteButton.setButtonText("Mute");
    muteButton.setClickingTogglesState(true);
    muteButton.setToggleState(false, dontSendNotification);
    muteButton.setColour(TextButton::buttonColourId, Colour(0xFF34495E));
    muteButton.setColour(TextButton::buttonOnColourId, Colour(0xFFC5EFF7));
    muteButton.setColour(TextButton::textColourOffId, Colour(0xFFBFBFBF));
    muteButton.setColour(TextButton::textColourOnId, Colour(0xFFECF0F1));
    muteButton.addListener(this);
    addAndMakeVisible(muteButton);
    
    resetButton.setButtonText("Reset");
    resetButton.setColour(TextButton::buttonColourId, Colour(0xFFE74C3C));
    resetButton.addListener(this);
    addAndMakeVisible(resetButton);
}




void OscillatorGUI:: initialiseLabels()
{
    noteAmplitudeLabel.setText("Amp", dontSendNotification);
    noteAmplitudeLabel.setColour(Label::ColourIds::textColourId, Colours::white);
    noteAmplitudeLabel.setFont(Font("Arial", 14, 0));
    addAndMakeVisible(noteAmplitudeLabel);
    
    attackLabel.setText("Att", dontSendNotification);
    attackLabel.setColour(Label::ColourIds::textColourId, Colours::white);
    attackLabel.setFont(Font("Arial", 14, 0));
    addAndMakeVisible(attackLabel);
    
    decayLabel.setText("Dec", dontSendNotification);
    decayLabel.setColour(Label::ColourIds::textColourId, Colours::white);
    decayLabel.setFont(Font("Arial", 14, 0));
    addAndMakeVisible(decayLabel);
}