//
//  Oscillator.cpp
//  JuceBasicAudio
//
//  Created by Leonard Terry on 10/11/2016.
//
//

#include "Oscillator.hpp"

Oscillator::Oscillator()
{
    reset();
}




Oscillator::~Oscillator()
{
    
}




void Oscillator::setAmplitude (float amp)
{
    amplitude = amp;
}




void Oscillator::setFrequency (float freq)
{
    frequency = freq;
    phaseInc = (2 * M_PI * frequency ) / sampleRate ;
}




void Oscillator::reset()
{
    phase = 0.f;
    sampleRate = 44100;
    setFrequency (440.f);
    setAmplitude (0.f);
}




void Oscillator::setSampleRate (float sr)
{
    sampleRate = sr;
    setFrequency (frequency); //just to update the phaseInc
}




float Oscillator::nextSample()
{
    float out = renderWaveShape(phase ) * amplitude;
    phase += phaseInc ;
    if(phase  > (2.f * M_PI))
        phase -= (2.f * M_PI);
    
    return out;
}