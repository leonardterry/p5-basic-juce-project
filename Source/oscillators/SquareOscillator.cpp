//
//  SquareOscillator.cpp
//  JuceBasicAudio
//
//  Created by Leonard Terry on 10/11/2016.
//
//

#include "SquareOscillator.hpp"

float SquareOscillator::renderWaveShape (const float currentPhase)
{
    float out;
    
    if (currentPhase <= M_PI){
        out = 1.0;
    }
    else if(currentPhase >= M_PI){
        out = -1.0;
    }
    else {
        out = 0.0;
    }
    
    return out * 0.3;
}