//
//  TriangleOscillator.cpp
//  JuceBasicAudio
//
//  Created by Leonard Terry on 10/11/2016.
//
//

#include "TriangleOscillator.hpp"

float TriangleOscillator::renderWaveShape (const float currentPhase)
{
    float out;
    
    if (currentPhase == 0){
        out = -1.0;
    }
    else if (currentPhase > 0 && currentPhase <= M_PI){
        out = ((2 * currentPhase) / M_PI) - 1.0;
    }
    else if (currentPhase > M_PI && currentPhase <= 2 * M_PI)
    {
        out = ((2 * - currentPhase) / M_PI) + 3;
    }
    
    return out;
}