//
//  OscillatorHeader.h
//  JuceBasicAudio
//
//  Created by Leonard Terry on 10/11/2016.
//
//

/** 
    This header file stores all the header files associated 
    with Oscillator and is called by Audio.h
 */

#ifndef OscillatorHeader_h
#define OscillatorHeader_h

#include "Oscillator.hpp"
#include "SinOscillator.hpp"
#include "SquareOscillator.hpp"
#include "SawtoothOscillator.hpp"
#include "TriangleOscillator.hpp"

#endif /* OscillatorHeader_h */
