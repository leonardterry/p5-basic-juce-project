//
//  NoteArray.cpp
//  JuceBasicWindow
//
//  Created by Leonard Terry on 22/11/2016.
//
//

#include "NoteArray.hpp"

NoteArray::NoteArray()
{
    
}




NoteArray::~NoteArray()
{
    
}




void NoteArray::resetAllArrays()
{
    noteArray.clear();
    originalArray.clear();
    upArray.clear();
    downArray.clear();
    alternateArray.clear();
}




void NoteArray::updateAllArrays()
{
    originalNoteArray();
    sortNoteArray();
    reverseNoteArray();
    alternateNoteArray();
}




void NoteArray::originalNoteArray()
{
    originalArray = noteArray;
}




void NoteArray::sortNoteArray()
{
    upArray = noteArray;
    upArray.sort(sorter);
}




void NoteArray::reverseNoteArray()
{
    downArray = noteArray;
    downArray.sort(reverseSorter);
}




void NoteArray::alternateNoteArray()
{
    alternateArray = upArray;
    for (int c = 0; c < getCurrentNoteArraySize(); c++)
        alternateArray.add(downArray[c]);
}




void NoteArray::addNoteToArray(int newNote)
{
    noteArray.add(newNote);
    counter->setCounterRange(getCurrentNoteArraySize());
    updateAllArrays();
}




void NoteArray::removeNoteFromArray(int newNote)
{
//    noteArray.remove (noteArray.indexOf(newNote));        //this removes a single note - bug when 'alternate' pattern is selected
    noteArray.removeAllInstancesOf(newNote);                //this line fixes the bug.
    counter->setCounterRange(getCurrentNoteArraySize());
    updateAllArrays();
}