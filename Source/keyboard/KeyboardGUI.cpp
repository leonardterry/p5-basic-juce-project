//
//  KeyboardGUI.cpp
//  JuceBasicWindow
//
//  Created by Leonard Terry on 22/11/2016.
//
//

#include "KeyboardGUI.hpp"

KeyboardGUI::KeyboardGUI(Sequence& s) : sequence(s),
                                        noteArray(s.getNoteArray()),
                                        counter(s.getCounter())
{
    /** initialise white note properties, then black note properties */
    for (int c = 0; c < numWhiteNotes; c++){
        whiteNotes[c].setClickingTogglesState(true);
        whiteNotes[c].setColour(TextButton::buttonColourId, Colours::white);
        whiteNotes[c].setColour(TextButton::buttonOnColourId, Colours::yellow);
        addAndMakeVisible(whiteNotes[c]);
        whiteNotes[c].addListener(this);
        
        whiteNoteIndicator[c].setColour(TextButton::buttonColourId, Colour(0xFF22313F));
        whiteNoteIndicator[c].setColour(TextButton::buttonOnColourId, Colour(0xFF2ECC71));
        addAndMakeVisible(whiteNoteIndicator[c]);
    }
    for (int c = 0; c < numBlackNotes; c++){
        blackNotes[c].setClickingTogglesState(true);
        blackNotes[c].setColour(TextButton::buttonColourId, Colours::black);
        blackNotes[c].setColour(TextButton::buttonOnColourId, Colours::yellow);
        addAndMakeVisible(blackNotes[c]);
        blackNotes[c].addListener(this);
        
        blackNoteIndicator[c].setColour(TextButton::buttonColourId, Colour(0xFF22313F));
        blackNoteIndicator[c].setColour(TextButton::buttonOnColourId, Colour(0xFF2ECC71));
        addAndMakeVisible(blackNoteIndicator[c]);
    }
    
    counter.addListener(this);
    resized();
}




KeyboardGUI::~KeyboardGUI()
{
    
}




void KeyboardGUI::resized()
{
    /** set white note bounds, then black note bounds */
    whiteNotes[0].setBoundsRelative(0.01, 0.1, 0.07, 0.8);
    whiteNoteIndicator[0].setBoundsRelative(0.01, 0.92, 0.07, 0.08);
    for (int c = 1; c < numWhiteNotes; c++) {
        whiteNotes[c].setBounds(whiteNotes[c-1].getRight(), whiteNotes[0].getY(), whiteNotes[0].getWidth(), whiteNotes[0].getHeight());
        whiteNoteIndicator[c].setBounds(whiteNoteIndicator[c-1].getRight(), whiteNoteIndicator[0].getY(), whiteNoteIndicator[0].getWidth(), whiteNoteIndicator[0].getHeight());
        
    }
    
    blackNotes[0].setBounds(whiteNotes[0].getRight()-(whiteNotes[0].getWidth()/2.0), whiteNotes[0].getY(), whiteNotes[0].getWidth(), whiteNotes[0].getHeight()/1.6);
    blackNoteIndicator[0].setBounds(whiteNotes[0].getRight()-(whiteNotes[0].getWidth()/2.0), 0.0, whiteNoteIndicator[0].getWidth(), whiteNoteIndicator[0].getHeight());
    for (int c = 1; c < numBlackNotes; c++)
    {        
        if (c == 2)
        {
            blackNotes[2].setBounds(blackNotes[c-1].getRight()+blackNotes[c-1].getWidth(), whiteNotes[0].getY(), whiteNotes[0].getWidth(), whiteNotes[0].getHeight()/1.6);
            blackNoteIndicator[2].setBounds(blackNoteIndicator[c-1].getRight()+blackNoteIndicator[c-1].getWidth(), 0.0, whiteNoteIndicator[0].getWidth(), whiteNoteIndicator[0].getHeight());
        }
        else if (c == 5)
        {
            blackNotes[5].setBounds(blackNotes[c-1].getRight()+blackNotes[c-1].getWidth(), whiteNotes[0].getY(), whiteNotes[0].getWidth(), whiteNotes[0].getHeight()/1.6);
            blackNoteIndicator[5].setBounds(blackNoteIndicator[c-1].getRight()+blackNoteIndicator[c-1].getWidth(), 0.0, whiteNoteIndicator[0].getWidth(), whiteNoteIndicator[0].getHeight());
        }
        else if (c == 7)
        {
            blackNotes[7].setBounds(blackNotes[c-1].getRight()+blackNotes[c-1].getWidth(), whiteNotes[0].getY(), whiteNotes[0].getWidth(), whiteNotes[0].getHeight()/1.6);
            blackNoteIndicator[7].setBounds(blackNoteIndicator[c-1].getRight()+blackNoteIndicator[c-1].getWidth(), 0.0, whiteNoteIndicator[0].getWidth(), whiteNoteIndicator[0].getHeight());
        }
        else
        {
            blackNotes[c].setBounds(blackNotes[c-1].getRight(), whiteNotes[0].getY(), whiteNotes[0].getWidth(), blackNotes[0].getHeight());
            blackNoteIndicator[c].setBounds(blackNoteIndicator[c-1].getRight(), 0.0, whiteNoteIndicator[0].getWidth(), whiteNoteIndicator[0].getHeight());
        }
    }
}




void KeyboardGUI::resetKeyboard()
{
    for (int c = 0; c < numWhiteNotes; c++)
        whiteNotes[c].setToggleState(false, dontSendNotification);
    for (int c = 0; c < numBlackNotes; c++)
        blackNotes[c].setToggleState(false, dontSendNotification);
    
    resetKeyboardIndicators();
}




void KeyboardGUI::resetKeyboardIndicators()
{
    for( int c = 0; c < numWhiteNotes; c++ )
        whiteNoteIndicator[c].setToggleState(false, dontSendNotification);
    for( int c = 0; c < numBlackNotes; c++ )
        blackNoteIndicator[c].setToggleState(false, dontSendNotification);
}




void KeyboardGUI::buttonClicked (Button* button)
{
    int whiteNoteMap[] = {60, 62, 64, 65, 67, 69, 71, 72, 74, 76, 77, 79, 81, 83};
    int blackNoteMap[] = {61, 63, 66, 68, 70, 73, 75, 78, 80, 82};
    
    int c = 0; //reset counter each time button is clicked
    
    //if button == whiteNotes[c] then break to if statements
    while (c < numWhiteNotes && button != &whiteNotes[c])
        c++;
    
    if (c < numWhiteNotes) //check that a white note was pressed
    {
        if (whiteNotes[c].getToggleState() == true)
        {
            noteArray.addNoteToArray (whiteNoteMap[c]);
            if (counter.isRunning())
                patternGUI->setOriginalPattern();
        }
        else
        {
            noteArray.removeNoteFromArray(whiteNoteMap[c]);
        }
    }
    else //find a match in the blacknotes
    {
        c = 0;
        while (c < numBlackNotes && button != &blackNotes[c])
            c++;
        
        if (c < numBlackNotes)
        {
            if (blackNotes[c].getToggleState() == true)
            {
                noteArray.addNoteToArray (blackNoteMap[c]);
                if (counter.isRunning())
                    patternGUI->setOriginalPattern();
            }
            else
            {
                noteArray.removeNoteFromArray(blackNoteMap[c]);
            }
        }
    }
}





void KeyboardGUI::counterChanged (const unsigned int counterValue)
{
    const MessageManagerLock mmLock;
    
    resetKeyboardIndicators();
    flashCurrentNote();
}




void KeyboardGUI::flashCurrentNote()
{
    switch (noteArray.getCurrentNoteNumber(counter.getCounterValue()))
    {
        case 60:
            whiteNoteIndicator[0].setToggleState(true, dontSendNotification);
            break;
        case 61:
            blackNoteIndicator[0].setToggleState(true, dontSendNotification);
            break;
        case 62:
            whiteNoteIndicator[1].setToggleState(true, dontSendNotification);
            break;
        case 63:
            blackNoteIndicator[1].setToggleState(true, dontSendNotification);
            break;
        case 64:
            whiteNoteIndicator[2].setToggleState(true, dontSendNotification);
            break;
        case 65:
            whiteNoteIndicator[3].setToggleState(true, dontSendNotification);
            break;
        case 66:
            blackNoteIndicator[2].setToggleState(true, dontSendNotification);
            break;
        case 67:
            whiteNoteIndicator[4].setToggleState(true, dontSendNotification);
            break;
        case 68:
            blackNoteIndicator[3].setToggleState(true, dontSendNotification);
            break;
        case 69:
            whiteNoteIndicator[5].setToggleState(true, dontSendNotification);
            break;
        case 70:
            blackNoteIndicator[4].setToggleState(true, dontSendNotification);
            break;
        case 71:
            whiteNoteIndicator[6].setToggleState(true, dontSendNotification);
            break;
        case 72:
            whiteNoteIndicator[7].setToggleState(true, dontSendNotification);
            break;
        case 73:
            blackNoteIndicator[5].setToggleState(true, dontSendNotification);
            break;
        case 74:
            whiteNoteIndicator[8].setToggleState(true, dontSendNotification);
            break;
        case 75:
            blackNoteIndicator[6].setToggleState(true, dontSendNotification);
            break;
        case 76:
            whiteNoteIndicator[9].setToggleState(true, dontSendNotification);
            break;
        case 77:
            whiteNoteIndicator[10].setToggleState(true, dontSendNotification);
            break;
        case 78:
            blackNoteIndicator[7].setToggleState(true, dontSendNotification);
            break;
        case 79:
            whiteNoteIndicator[11].setToggleState(true, dontSendNotification);
            break;
        case 80:
            blackNoteIndicator[8].setToggleState(true, dontSendNotification);
            break;
        case 81:
            whiteNoteIndicator[12].setToggleState(true, dontSendNotification);
            break;
        case 82:
            blackNoteIndicator[9].setToggleState(true, dontSendNotification);
            break;
        case 83:
            whiteNoteIndicator[13].setToggleState(true, dontSendNotification);
            break;
        default:
            break;
    }
}
