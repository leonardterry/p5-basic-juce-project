//
//  Sequence.cpp
//  JuceBasicWindow
//
//  Created by Leonard Terry on 22/11/2016.
//
//

#include "Sequence.hpp"

Sequence::Sequence(Audio& a) :  audio(a)
{
    counter.addListener(this);
    noteArray.passReference(&counter);
}





Sequence::~Sequence()
{
    
}




void Sequence::counterChanged(const unsigned int counterValue)
{
    audio.beep(noteArray.getCurrentNoteNumber(counter.getCounterValue()) + sequenceOctave);
}
