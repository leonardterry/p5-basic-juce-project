//
//  KeyboardGUI.hpp
//  JuceBasicWindow
//
//  Created by Leonard Terry on 22/11/2016.
//
//

#ifndef KeyboardGUI_hpp
#define KeyboardGUI_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "NoteArray.hpp"
#include "Sequence.hpp"
#include "Counter.hpp"
#include "PatternGUI.hpp"

/**
    This class creates the UI for all keyboard related settings.
 
    It creates two arrays of text buttons for white and black notes of a keyboard

    When a button is clicked it loops to find which button has been clicked and tells sequence
    to add a note to the noteArray.

    The KeyboardGUI object references the Note Array to send note number values to be added 
    and removed from the array. It also references the sequence object to be a listener, 
    when the sequence changes it sends current step and current note.
 */

class KeyboardGUI :     public Component,
                        public Button::Listener,
                        public Counter::Listener
{
public:
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /** Constructor */
    KeyboardGUI(Sequence& s);
    
    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /** Destructor */
    ~KeyboardGUI();

    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /**
        Resizes the components within the class.
     */
    void resized() override;
    
    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /**
        This function passes a pointer reference to MainComponent, to be used by PatternGUI.
        MainComponent calls this function in its constructor to complete the reference.
     */
    void passReference(PatternGUI *ref)                            { patternGUI = ref; }

    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /**
        Resets all buttons that make up the keyboard interface to false and triggers 'resetKeyboardIndicators'.
     */
    void resetKeyboard();
    
    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /**
        Resets all the keyboard indicators to false.
     */
    void resetKeyboardIndicators();
    
    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /**
        When a keyboard button is pressed, this function checks a series of conditional statements that checks 
        through all buttons to find which has been clicked, and it adds a note number to noteArray.
     */
    void buttonClicked (Button* button) override;
    
    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /**
        Resets keyboard indicators and triggers 'flashCurrentNote'.
     */
    void counterChanged (const unsigned int counterValue) override;
    
    
    //≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠
    /**
        Sets the toggle state for the current note in the sequence to 'true' in order to flash the indicator.
     */
    void flashCurrentNote();

    
private:
    static const int numWhiteNotes = 14;
    static const int numBlackNotes = 10;
    
    TextButton whiteNotes[numWhiteNotes];
    TextButton blackNotes[numBlackNotes];
    TextButton whiteNoteIndicator[numWhiteNotes];
    TextButton blackNoteIndicator[numBlackNotes];
    
    Sequence& sequence; //references sequence directly
    NoteArray& noteArray; //references note array through sequence
    Counter& counter; //references counter through sequence
    PatternGUI* patternGUI;
};

#endif /* KeyboardGUI_hpp */
